
package com.excercise.fromjson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AppendListExcercise
{

    private List<Excercise> excercise = new ArrayList<Excercise>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public AppendListExcercise() {
    }

    /**
     * 
     * 
     */
    public AppendListExcercise(List<Excercise> excercise) {
        this.excercise = excercise;
       
    }

    /**
     * 
     * @return
     *     The excercise
     */
    public List<Excercise> getExcercise() {
        return this.excercise;
    }

    /**
     * 
     * @param excercise
     *     The excercise
     */
    public void setExcercise(List<Excercise> excercise) {
        this.excercise = excercise;
    }

    public AppendListExcercise withExcercise(List<Excercise> excercise) {
        this.excercise = excercise;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AppendListExcercise withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
