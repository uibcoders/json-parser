package com.excercise.fromjson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Parser {

	private static final String filePath = ".\\output_2_main.json";
	
	public static void main(String[] args) {

		try {
			// read the json file
			FileReader reader = new FileReader(filePath);

			JSONParser jsonParser = new JSONParser();
			JSONArray jsonArray = (JSONArray) jsonParser.parse(reader);
			int count = 0;
			for(int i=0; i<jsonArray.size(); i++){
				//System.out.println("The " + i + " element of the array: "+jsonArray.get(i));
			
				count++;
				String innerObj = remove(jsonArray.get(i).toString());
				String [] objects  = innerObj.split("[:]+", 2);
				JSONArray innerObjArray = (JSONArray) jsonParser.parse(objects[1]);

				
				//System.out.println(jsonObject.toString());
				
				Iterator<?> i1 = innerObjArray.iterator();

				// take each value from the json array separately
				while (i1.hasNext()) {
					JSONObject innerObj2 = (JSONObject) i1.next();
					//System.out.println(innerObj2);
					System.out.println();
					System.out.println();
					System.out.println( objects[0]);
					System.out.println();
					System.out.println("link --- "+ innerObj2.get("link"));
					System.out.println("Main Muscle Worked --- "+ innerObj2.get("Main Muscle Worked")); 
					System.out.println("Other Muscles --- "+ innerObj2.get("Other Muscles")); 

					System.out.println("Mechanics Type --- "+ innerObj2.get("Mechanics Type"));  
					System.out.println("Type --- "+ innerObj2.get("Type")); 
					System.out.println("Level --- "+ innerObj2.get("Level")); 
					System.out.println("Equipment --- "+ innerObj2.get("Equipment")); 
					System.out.println("Force --- "+ innerObj2.get("Force")); 
					System.out.println("male_image_start --- "+ innerObj2.get("male_image_start")); 
					System.out.println("male_image_end --- "+ innerObj2.get("male_image_end")); 
					System.out.println("female_image_start --- "+ innerObj2.get("female_image_start")); 
					System.out.println("female_image_end --- "+ innerObj2.get("female_image_end")); 
					System.out.println("mucle_heatmap --- "+ innerObj2.get("mucle_heatmap")); 
					
			
				
					System.out.println("guide --- "+ innerObj2.get("guide").toString().substring(1, innerObj2.get("guide").toString().length() -1)); 
				
				}
				
			}
			
System.out.println("Count: " + count);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}

	}
	public static String remove(String str) {
	    if (str.length() > 0) {
	      str =  str.substring(1, str.length()-1) ;
	      
	    }
	    
	    return str;
	}

}